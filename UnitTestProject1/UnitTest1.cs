﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTestProject1
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestDecrementationBon()
        {
            int valeur = 50;
            Assert.AreEqual(ClassLibrary1.CounterClass.Decrementation(valeur), 49);
        }

        [TestMethod]
        public void TestDecrementationFaux()
        {
            int valeur = 51;
            Assert.AreEqual(ClassLibrary1.CounterClass.Decrementation(valeur), 49);
        }

        [TestMethod]
        public void TestIncrementationBon()
        {
            int valeur = 50;
            Assert.AreEqual(ClassLibrary1.CounterClass.Incrementation(valeur), 51);
        }

        [TestMethod]
        public void TestIncrementationFaux()
        {
            int valeur = 51;
            Assert.AreEqual(ClassLibrary1.CounterClass.Incrementation(valeur), 51);
        }

        [TestMethod]
        public void TestRàZBon()
        {
            int valeur = 50 ;
            Assert.AreEqual(ClassLibrary1.CounterClass.RàZ(valeur), 0);
        }

        [TestMethod]
        public void TestRàZFaux()
        {
            int valeur = 50;
            Assert.AreEqual(ClassLibrary1.CounterClass.RàZ(valeur), 50);
        }
    }
}
