﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Basic_Counter
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lb_total = New System.Windows.Forms.Label()
        Me.btn_moins = New System.Windows.Forms.Button()
        Me.btn_plus = New System.Windows.Forms.Button()
        Me.tb_increment = New System.Windows.Forms.TextBox()
        Me.btn_RàZ = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'lb_total
        '
        Me.lb_total.AutoSize = True
        Me.lb_total.Location = New System.Drawing.Point(384, 75)
        Me.lb_total.Name = "lb_total"
        Me.lb_total.Size = New System.Drawing.Size(48, 17)
        Me.lb_total.TabIndex = 0
        Me.lb_total.Text = "Total :"
        '
        'btn_moins
        '
        Me.btn_moins.Location = New System.Drawing.Point(49, 211)
        Me.btn_moins.Name = "btn_moins"
        Me.btn_moins.Size = New System.Drawing.Size(75, 23)
        Me.btn_moins.TabIndex = 1
        Me.btn_moins.Text = "-"
        Me.btn_moins.UseVisualStyleBackColor = True
        '
        'btn_plus
        '
        Me.btn_plus.Location = New System.Drawing.Point(676, 211)
        Me.btn_plus.Name = "btn_plus"
        Me.btn_plus.Size = New System.Drawing.Size(75, 23)
        Me.btn_plus.TabIndex = 2
        Me.btn_plus.Text = "+"
        Me.btn_plus.UseVisualStyleBackColor = True
        '
        'tb_increment
        '
        Me.tb_increment.Location = New System.Drawing.Point(357, 211)
        Me.tb_increment.Name = "tb_increment"
        Me.tb_increment.Size = New System.Drawing.Size(100, 22)
        Me.tb_increment.TabIndex = 3
        Me.tb_increment.Text = "0"
        '
        'btn_RàZ
        '
        Me.btn_RàZ.Location = New System.Drawing.Point(357, 356)
        Me.btn_RàZ.Name = "btn_RàZ"
        Me.btn_RàZ.Size = New System.Drawing.Size(75, 23)
        Me.btn_RàZ.TabIndex = 4
        Me.btn_RàZ.Text = "RàZ"
        Me.btn_RàZ.UseVisualStyleBackColor = True
        '
        'Basic_Counter
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(800, 450)
        Me.Controls.Add(Me.btn_RàZ)
        Me.Controls.Add(Me.tb_increment)
        Me.Controls.Add(Me.btn_plus)
        Me.Controls.Add(Me.btn_moins)
        Me.Controls.Add(Me.lb_total)
        Me.Name = "Basic_Counter"
        Me.Text = "Basic_Counter"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents lb_total As Label
    Friend WithEvents btn_moins As Button
    Friend WithEvents btn_plus As Button
    Friend WithEvents tb_increment As TextBox
    Friend WithEvents btn_RàZ As Button
End Class
