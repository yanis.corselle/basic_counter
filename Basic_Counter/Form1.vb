﻿
Imports ClassLibrary1

Public Class Basic_Counter
    Private Sub Btn_moins_Click(sender As Object, e As EventArgs) Handles btn_moins.Click
        tb_increment.Text = CounterClass.Decrementation(tb_increment.Text)

    End Sub

    Private Sub Btn_plus_Click(sender As Object, e As EventArgs) Handles btn_plus.Click
        tb_increment.Text = CounterClass.Incrementation(tb_increment.Text)

    End Sub

    Private Sub Btn_RàZ_Click(sender As Object, e As EventArgs) Handles btn_RàZ.Click
        tb_increment.Text = CounterClass.RàZ(tb_increment.Text)

    End Sub
End Class