﻿using System;

namespace ClassLibrary1
{
    public class CounterClass
    {
        int compteur;

        public static int Incrementation(int compteur)
        {
            compteur++;
            return compteur;
        }

        public static int Decrementation(int compteur)
        {
            compteur--;
            return compteur;
        }

        public static int RàZ(int compteur)
        {
            compteur = 0;
            return compteur;
        }

    }
}
